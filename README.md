# README #
AngularTest WebApp

## Development server ##
Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files


### How do I get set up? ###

Step 1 Clone repository     git clone https://mailtojig@bitbucket.org/mailtojig/angulartest.git  
Step 2 Navigate to that directory  > npm intall  


### How to run web app ###
Run the application in the browser with disabled websecurity (Google Chrome)
 
 Step 1 Windows + R (Open Run)<br/>
 
 Step 2 Enter below string into run  
 	chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security  (open new browser window)  
         
 Step 3 npm start	
     Navigate to http://localhost:4200/.
 
