import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatFormField, MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { MemberService } from './member.service';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';

import { Member } from './member';
import { MemberResponse } from './member.res';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],

  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class MembersComponent implements AfterViewInit {
  resultsLength = 0;
  isLoadingResults = true;
  columnsToDisplay = [
    { map: '_id', label: 'Id' },
    { map: 'officialName', label: 'Official Name' },
    { map: 'active', label: 'Active Status' },
    { map: 'givenName', label: 'Given Name' }
   
  ];
  columnsMap = this.columnsToDisplay.map(c => c.map);

  voteColumns = [
    { map: 'endDate', label: 'Date' },
    { map: 'vote', label: 'Vote' },
    { map: 'votename', label: 'Name' }];

  voteColumnsMap =  this.voteColumns.map(c => c.map);

  dataSource;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  constructor(private memberService: MemberService) {}

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.memberService.getMembers(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex
          );
        }),
        map(data => {
          this.isLoadingResults = false;
          this.resultsLength = data.pagination.count;

          return data.results;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          return observableOf([]);
        })
      )
      .subscribe(data => {

      this.memberService.getVotes().then( result => {
        const allVotes = result['results'];
        data.forEach(member => {
          member.votes = [];
          allVotes.forEach(vote => {
            const memberFound = vote.members.find(mem => mem._id == vote.usCongressBio);
            if (memberFound) {
              member.votes.push({
                votename: vote.name,
                description: vote.description,
                vote: memberFound.vote,
                endDate: vote.endDate
              });
            }
          });
        });
      }).catch( error => {
        console.log('Error Getting Data: ', error);
      });
      this.dataSource = new MatTableDataSource(data);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
