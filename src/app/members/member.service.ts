import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MemberResponse } from './member.res';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private membersUrl = 'https://clerkapi.azure-api.net/Members';

  constructor(
    private http: HttpClient
  ) {}

  /** GET members from the server */
  getMembers(isSorted: string, sortDirection: string, pageIndex: number): Observable<MemberResponse> {
    // Filtering to fetch only active members
    let params = {
      $filter : 'active eq \'yes\''
    };

    if (pageIndex > 0) {
      // Get this 10 from component so it remain in sync with view
      params['$skip'] = pageIndex * 10;
    }

    return this.http.get<MemberResponse>(this.membersUrl, { params }).pipe(
      catchError(this.handleError<MemberResponse>('getMembers'))
    );
  }

  getVotes(): Promise<any[]> {
      // Using local JOSN as votes data is heavy file for our current use case
      return this.http.get<any[]>('http://localhost:4200/assets/votes.json').toPromise();
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
}
