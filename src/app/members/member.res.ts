import {Member} from './member';

export class MemberResponse {
    results: Array<Member>;
// tslint:disable-next-line: ban-types
    pagination: {
        page: number;
        count: number;
        per_page: number;
        number_pages: number;
        time: string;
        target: string;
    };
}
