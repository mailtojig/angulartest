import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MembersComponent } from '../app/members/members.component';

//Navigation Menu routes 
const routes: Routes =  [
  { path: 'members', component: MembersComponent},
  { path: '', redirectTo: '/members', pathMatch: 'full' }
 ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
